<?php

class NetworkAdmin extends LeftAndMain implements PermissionProvider {
	private static $url_segment = "network";
	private static $menu_title = "Network";	
	private static $menu_icon = 'network/images/network-icon.png';
	
	private static $allowed_actions = array(
		'root_data',
		'load_data',
		'sponsor_member_detail',
		'build_tree',
		'placement_member_detail',
		'placement_signup'
	);

	public function init() {
		parent::init();
		Requirements::javascript(THIRDPARTY_DIR . '/jquery/jquery.js');
		Requirements::javascript(FRAMEWORK_DIR . '/javascript/i18n.js');
		Requirements::add_i18n_javascript('network/javascript/lang');
		Requirements::javascript('network/javascript/NetworkAdmin.min.js');
		Requirements::css('network/css/NetworkAdmin.css');
	}
	
	function Sponsor(){
		return Distributor::get_obj_by_username(Sponsor::get_root_username());
	}
	
	function Placement(){
		return Distributor::get_obj_by_username(Placement::get_root_username());
	}
	
	function root_data(){
		$root_id = $this->Sponsor()->ID;
		if($this->request->getVar('username')){
			$sponsor = Sponsor::get()->filter('Member.Username', $this->request->getVar('username'))->first();
			$root_id = $sponsor ? $sponsor->ID : 0;
		}

		if($root_id && $sponsor = Sponsor::get()->byID($root_id)){
			$root = array(
				$sponsor->NLeft => Sponsor::tree_node($root_id)
			);
			$parentid = $sponsor->ParentID;
			while($parentid && $sponsor = Sponsor::get()->byID($parentid)){
				$root[$sponsor->NLeft] = array_merge(Sponsor::tree_node($parentid), array('state' => ''));
				$parentid = $sponsor->ParentID;
			}
			ksort($root);
			$root = array_values($root);
			$data = array(
				'result' => 'success',
	            'root' => $root,
	            'root_id' => $root_id
	        );
		}
		else{
			$data = array(
				'result' => 'error',
	            'msg' => _t('NetworkAdmin.INVALID_MEMBER', 'Invalid member')
	        );
		}
		
        return Convert::array2json($data);
    }
	
	function load_data(){
        if($this->request->getVar('operation') && method_exists($this, $this->request->getVar('operation'))) {
            $this->response->addHeader("Content-Type", "application/json; charset=utf-8");
            $this->response->addHeader("Pragma", "no-cache");
            $this->response->addHeader("Cache-Control", "no-cache, must-revalidate");
            $this->response->setStatusCode(200);
            return $this->{$this->request->getVar('operation')}($this->request->getVar('id'));
        }
        
        $this->response->setStatusCode(404);
    }
	
	function sponsor_member_detail(){
        $current_sponsor = Sponsor::get()->find('MemberID', (int)$this->Sponsor()->ID);
        $sponsor = Sponsor::get()->filter('NLeft:GreaterThanOrEqual', $current_sponsor->NLeft)->filter('NRight:LessThanOrEqual', $current_sponsor->NRight)->find('MemberID', (int)$this->request->getVar('id'));
        if($sponsor){
            $member = Member::get()->byID($sponsor->MemberID);
            $template = new SSViewer('NetworkAdmin_sponsor_member_details');
	        $template->includeRequirements(false);
	        return $template->process($this->customise(array('Member' => $member)));
        }
        
        return _t('NetworkAdmin.INVALID_MEMBER', 'Invalid member');
    }
    
    function Search($username){
        $sponsor = Sponsor::get()->find('MemberID', (int)Distributor::currentUserID());
        $data = array();
		$matchedDownlines = Sponsor::get()->filter('NLeft:GreaterThanOrEqual', $sponsor->NLeft)->filter('NRight:LessThanOrEqual', $sponsor->NRight)->filter('Member.Username:PartialMatch', $username);
		if(!$matchedDownlines){
			return "[]";
		}
		
		$filter = array();
		foreach($matchedDownlines as $item){
			$filter[] = " (Sponsor.NLeft < ".(int)$item->NLeft." AND Sponsor.NRight > ".(int)$item->NRight.") ";
		}
		
		$select = array(
			"`ID`"
		);
		$from = array(
			"`Sponsor`"
		);
		$where = $filter;
		$orderby = '';

		$query = new SQLQuery($select, $from, $where, $orderby);
		$query->setDistinct(true);
		$query->setConnective('OR');
		$result = $query->execute();
		$data = array();
		while($item = $result->next()) { $data[] = "#node_".$item['ID']; }
        return Convert::array2json($data);
    }
    
    function GetChildren($id){
        $sponsor = Sponsor::get()->byID((int)$id);
        if($sponsor){
            $result = array();
            $level = $sponsor->NLevel + 1;
            $children = $sponsor->ChildBranch("NLevel = ".$level);
            foreach($children as $child){
                $result[] = Sponsor::tree_node($child->ID);
            }
            return Convert::array2json($result);
        }
    }
	
	function build_tree(){
        $username = $this->request->getVar('username');

        $id = (int)Distributor::get_id_by_username($username);
        $left = 0;
        $right = 0;
        $placement = DataObject::get_one('Placement', "MemberID = ".(int)$this->Placement()->ID);
        if($placement){
            $left = (int)$placement->NLeft;
            $right = (int)$placement->NRight;
        }

        $data = Placement::get()
        ->filter('NLeft:GreaterThanOrEqual', $left)
        ->filter('NRight:LessThanOrEqual', $right)
        ->find('ID', $id);
		
		if(!$data){
			 return Convert::array2json(array('error' => _t('NetworkAdmin.UNABLE_FIND_TEAM_NETWORK', 'Unable find team network for member username - {username}', '', array('username' => $username))));
		}

        $level = (int)$data->NLevel;
        $left = (int)$data->NLeft;
        $right = (int)$data->NRight;
        $parentid = (int)$data->ParentID;
        $parentusername = ($data->ParentID) ? $data->Parent()->Member()->Username : '';
        $position = (int)$data->NSeqno;
        $showlevel = Placement::get_show_level();
        $maxLevel = $level + $showlevel;
        
        $items = Placement::get()
        ->filter('NLeft:GreaterThanOrEqual', $left)
        ->filter('NRight:LessThanOrEqual', $right)
        ->filter('NLevel:LessThanOrEqual', $maxLevel)
        ->sort(array('NLevel' => 'ASC', 'NSeqno' => 'ASC', 'ID' => 'ASC'));

        $searchResult = array();
        if($items->count()){
            foreach($items as $key => $item){
                if($item->MemberID){
                    $member = $item->Member();
                    $arr_data = array();
                    $arr_data['id'] = $item->ID;
                    $arr_data['level'] = (int)($item->NLevel - $level);
                    $arr_data['parentid'] = (int)$item->ParentID;
                    $arr_data['position'] = (int)$item->NSeqno;
                    $arr_data['distributorid'] = (int)$member->ID;
                    $arr_data['rankcode'] = $member->RankCode;
                    $arr_data['username'] = ($member->Username) ? $member->Username : 'n/a';
					$arr_data['left'] = DBField::create_field('Int', $member->TodayTotalLeftGroupBV)->Formatted();
					$arr_data['right'] = DBField::create_field('Int', $member->TodayTotalRightGroupBV)->Formatted();
                    if(isset($searchResult[$arr_data['level']][$arr_data['parentid']][$arr_data['position']])){
                        continue;
                    }
                    
                    $searchResult[$arr_data['level']][$arr_data['parentid']][$arr_data['position']] = $arr_data;
                }
            }
        }

        $currentcol = 1;
        $child_limit = Placement::get_direct_child_limit();
        $totalcol = 1;
        for ($k = 1; $k < $showlevel; $k++) {
            $totalcol *= $child_limit;
        }
        $table = '';
        $arr_postparentid = array();
        $disabled = '';
        $currentusername = $this->Placement()->Username;
        $currentmemberid = $this->Placement()->ID;
        $currentmemberrankcode = $this->Placement()->RankCode;
		$currentleft = DBField::create_field('Int', $this->Placement()->TodayTotalLeftGroupBV)->Formatted();
		$currentright = DBField::create_field('Int', $this->Placement()->TodayTotalRightGroupBV)->Formatted();
        
        if($username == $currentusername){
            $disabled = "disabled";
            $parentusername = "";
        }
        $table .= '<p><a href="#" rel="tooltip" title="'._t('NetworkAdmin.VIEW_PREVIOUS_LEVEL', 'View previous level of team network').'" id="previous" data-username="'.$parentusername.'" class="btn action '.$disabled.'">'._t('NetworkAdmin.PREVIOUS_LEVEL', 'Previous Level').'</a></p>';
        $table .= '<div class="team-table">';
        $table .= '<table width="100%" class="table table-striped">';
        $table .= '<tbody>';
        if($username != $currentusername){
            $table .= '<tr>';
            $table .= '<td colspan="'.$totalcol.'" class="text-center">';
            $table .= '<div><a class="view_tree" href="#" data-username="'.$currentusername.'"><span class="ui-icon ui-icon-arrowthickstop-1-n" style="display:inline-block; vertical-align: middle;"></span><br><b>'._t('NetworkAdmin.TOP', 'TOP').'</b></a></div>';
            $data = array(
				'MemberLink' => Controller::join_links($this->Link('placement_member_detail'), $currentmemberid),
				'Member' => Distributor::get_obj_by_username($currentusername),
				'Image' => $this->RankImageURL($currentmemberrankcode)
			);
			$table .= $this->generate_node($data);
            $table .= '<p>.</p>';
			$table .= '<p><a class="view_tree" href="#" data-username="'.$parentusername.'"><span class="ui-icon ui-icon-arrowthickstop-1-n" style="display:inline-block; vertical-align: middle;"></span></a></p>';
            $table .= '</td>';
            $table .= '</tr>';
        }
        for ($i = 0; $i < $showlevel; $i++) {
            $parentkey = 0;
            $arr_parentid = array();
            $arr_data_by_level = isset($searchResult[$i]) ? $searchResult[$i] : array();
            $colspan = $totalcol/$currentcol;
            $table .= '<tr class="team_level'.$i.'">';
            $currentparentid = 0;
			$currentposition = 0;
            for ($j = 1; $j <= $currentcol; $j++) {
                $arr_data = array();
                if($i == 0){
                    $arr_data = isset($arr_data_by_level[$parentid][$position]) ? $arr_data_by_level[$parentid][$position] : array();
                }
                else{
                	$currentposition += 1;
					
                    if(isset($arr_postparentid[$parentkey]) && $currentparentid = (int)$arr_postparentid[$parentkey]){
                        if(isset($arr_data_by_level[$currentparentid][$currentposition])){
                            $arr_data = $arr_data_by_level[$currentparentid][$currentposition];
                        }
                    }
                    
                    if($currentposition >= $child_limit){
                        $parentkey += 1;
                    }
                }
                if(sizeof($arr_data)){
                    $table .= '<td colspan="'.$colspan.'" class="text-center">';
                    $data = array(
						'MemberLink' => Controller::join_links($this->Link('placement_member_detail'), $arr_data['distributorid']),
						'Member' => Distributor::get_obj_by_username($arr_data['username']),
						'Image' => $this->RankImageURL($arr_data['rankcode'])
					);
					$table .= $this->generate_node($data);
					if($i+1 == $showlevel){
						$table .= '<br><a class="view_tree" href="#" data-username="'.$arr_data['username'].'"><span class="ui-icon ui-icon-arrowthickstop-1-s" style="display:inline-block; vertical-align: middle;"></span></a>';
					}
                    $table .= "</td>";
                        
                    $arr_parentid[] = $arr_data['id'];
                }
                else{
                    $table .= '<td colspan="'.$colspan.'" class="text-center">';
					if($currentparentid && Permission::check('CMS_ACCESS_MemberAdmin') && Permission::check('CREATE_Distributor')){
						$url = Controller::join_links($this->Link('placement_signup'), sprintf('?username=%s&position=%s', Distributor::get_username_by_id($currentparentid), $currentposition));
						$data = array(
							'ParentID' => $currentparentid,
							'Link' => $url,
							'Image' => $this->RankImageURL('register'),
							'Position' => $currentposition
						);
						$table .= $this->generate_node($data);
                    }
                    else{
	                    $data = array(
	                		'ParentID' => 0,
	                		'Link' => '',
							'Image' => $this->RankImageURL('empty')
						);
						$table .= $this->generate_node($data);
					}

                    $table .= '</td>';
                    
                    $arr_parentid[] = 0;
                }

				if(!($j % $child_limit)){
                	$currentposition = 0;
				}
            }
            $table .= '</tr>';
            $currentcol *= $child_limit;
            $arr_postparentid = $arr_parentid;
        }
        $table .= '</tbody>';
        $table .= '</table>';
        $table .= '</div>';
        
        return Convert::array2json(array('result' => $table));
    }

	protected function generate_node($data){
		$template = new SSViewer('NetworkAdmin_generate_node');
        $template->includeRequirements(false);
        return $template->process($this->customise($data));
	}

	function placement_member_detail(){
        $current_placement = Placement::get()->find('MemberID', (int)$this->Placement()->ID);
        $placement = Placement::get()->filter('NLeft:GreaterThanOrEqual', $current_placement->NLeft)->filter('NRight:LessThanOrEqual', $current_placement->NRight)->find('MemberID', (int)$this->request->param('ID'));
        if($placement){
            $member = Member::get()->byID($placement->MemberID);
			
	        $template = new SSViewer('NetworkAdmin_placement_member_details');
	        $template->includeRequirements(false);
	        return $template->process($this->customise(array('Member' => $member)));
        }
        
        return _t('NetworkAdmin.INVALID_MEMBER', 'Invalid member');
    }
	
	function placement_signup(){
		Session::set('Backend_SponsorUsername', $this->request->getVar('username'));
		Session::set('Backend_PlacementUsername', $this->request->getVar('username'));
		Session::set('Backend_PlacementPosition', $this->request->getVar('position'));
		$url = Controller::join_links(MemberAdmin::create()->Link('Member'), 'EditForm', 'field', 'Member', 'item', 'new');
		return $this->redirect($url);
	}

	function RankImageURL($rankcode){
        $theme_dir = sprintf('%s/images/placement/', SSViewer::get_theme_folder());
        $icon = $rankcode ? sprintf('%s_icon.png', strtolower($rankcode)) : 'unknow_icon.png';
        if(Director::fileExists($theme_dir.$icon)){
            return $theme_dir.$icon;
        }
		else if(Director::fileExists('network/images/placement/'.$icon)){
			return 'network/images/placement/'.$icon;
		}
		else{
			if(Director::fileExists($theme_dir.'unknow_icon.png')){
	            return $theme_dir.'unknow_icon.png';
	        }
			else {
				return 'network/images/placement/unknow_icon.png';
			}
		}
    }
	
	public function providePermissions() {
		return array(
            'CMS_ACCESS_NetworkAdmin' => array(
                'name' => _t('NetworkAdmin.PERMISSION_VIEW_NETWORK', 'Allow view network tree'),
                'category' => _t('NetworkAdmin.PERMISSIONS_CATEGORY', 'Network')
            )
        );
	}
}