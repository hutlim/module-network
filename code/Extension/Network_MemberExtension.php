<?php

class Network_MemberExtension extends DataExtension implements PermissionProvider {
	private static $summary_fields = array(
		'Sponsor.Username'
    );
	
    private static $casting = array(
        'TotalSponsorDirectDownline' => 'Int',
        'TotalSponsorGroupDownline' => 'Int',
        'TotalPlacementDirectDownline' => 'Int',
        'TotalPlacementGroupDownline' => 'Int',
        'TotalPlacementLeftGroupDownline' => 'Int',
        'TotalPlacementRightGroupDownline' => 'Int'
    );
	
	function updateFieldLabels(&$labels) {
        $labels['Sponsor.Username'] = _t('Network_MemberExtension.SPONSOR_USERNAME', 'Sponsor Username');
	}
	
	function validate(ValidationResult $validationResult) {
		if($this->owner->hasField('SetSponsorUsername')){
			$current_sponsor = Sponsor::get()->filter('MemberID', $this->owner->ID)->filter('ParentID', Distributor::get_id_by_username($this->owner->SetSponsorUsername))->count();
			if(!$current_sponsor){
				$sponsor = Sponsor::get()->filter('Member.Username', $this->owner->SetSponsorUsername)->first();
				if(!$sponsor){
					$subvalid = new ValidationResult();
	        		$subvalid->error(_t('Network_MemberExtension.INVALID_SPONSOR_USERNAME', 'Invalid sponsor username'), "INVALID_SPONSOR_USERNAME");
	        		$validationResult->combineAnd($subvalid);
				}
				else if($this->owner->SetSponsorUsername == $this->owner->Username){
					$subvalid = new ValidationResult();
	        		$subvalid->error(_t('Network_MemberExtension.INVALID_SPONSOR_OWNSELF', 'Unable to sponsor ownself'), "INVALID_SPONSOR_OWNSELF");
	        		$validationResult->combineAnd($subvalid);
				}
				else if($this->owner->Placement()->ID){
					$obj = Placement::get()->find('MemberID', $sponsor->MemberID);
					$checkPlacement = Placement::get()
		        	->filter('NLeft:GreaterThanOrEqual', (int)$obj->NLeft)
		        	->filter('NRight:LessThanOrEqual', (int)$obj->NRight)
					->filter('MemberID', (int)$this->owner->Placement()->ID)->count();
					if(!$checkPlacement){
						$subvalid = new ValidationResult();
	            		$subvalid->error(_t('Network_MemberExtension.SPONSOR_NOT_MATCH', 'Sponsor is not match with the placement'), "SPONSOR_NOT_MATCH");
	            		$validationResult->combineAnd($subvalid);
					}
				}
			}
		}

		if($this->owner->hasField('SetPlacementUsername')){
			$current_placement = Placement::get()->filter('MemberID', $this->owner->ID)->filter('ParentID', Distributor::get_id_by_username($this->owner->SetPlacementUsername))->count();
			if(!$current_placement){
				$placement = Placement::get()->filter('Member.Username', $this->owner->SetPlacementUsername)->first();
				if(!$placement){
					$subvalid = new ValidationResult();
	        		$subvalid->error(_t('Network_MemberExtension.INVALID_PLACEMENT_USERNAME', 'Invalid placement username'), "INVALID_PLACEMENT_USERNAME");
	        		$validationResult->combineAnd($subvalid);
				}
				else if($this->owner->SetPlacementUsername == $this->owner->Username){
					$subvalid = new ValidationResult();
	        		$subvalid->error(_t('Network_MemberExtension.INVALID_PLACEMENT_OWNSELF', 'Unable to placement ownself'), "PLACEMENT_USERNAME_INVALID");
	        		$validationResult->combineAnd($subvalid);
				}
				else if($this->owner->SetPlacementPosition && Placement::get()->filter('ParentID', $placement->ID)->filter('NSeqno', $this->owner->SetPlacementPosition)->count()){
					$subvalid = new ValidationResult();
	        		$subvalid->error(_t('Network_MemberExtension.POSITION_EXISTS', 'The placement position is already registered by other member'), "POSITION_EXISTS");
	        		$validationResult->combineAnd($subvalid);
				}
				else if($this->owner->Sponsor()->ID){
					$obj = Placement::get()->find('MemberID', $this->owner->Sponsor()->ID);
					$checkPlacement = Placement::get()
		        	->filter('NLeft:GreaterThanOrEqual', (int)$obj->NLeft)
		        	->filter('NRight:LessThanOrEqual', (int)$obj->NRight)
					->filter('MemberID', (int)$this->owner->Placement()->ID)->count();
					if(!$checkPlacement){
						$subvalid = new ValidationResult();
	            		$subvalid->error(_t('Network_MemberExtension.PLACEMENT_NOT_MATCH', 'Placement is not match with the sponsor'), "PLACEMENT_NOT_MATCH");
	            		$validationResult->combineAnd($subvalid);
					}
				}
			}
		}
        
        return $validationResult;
    }
    
    function updateCMSFields(FieldList $fields){
        if(!$this->owner->exists()){
			$fields->insertAfter(PlacementField::create('SetPlacementUsername', _t('Network_MemberExtension.PLACEMENT_USERNAME', 'Placement Username'), Session::get('Backend_PlacementUsername'), Session::get('Backend_PlacementPosition')), 'Username');
            $fields->insertAfter(SponsorField::create('SetSponsorUsername', _t('Network_MemberExtension.SPONSOR_USERNAME', 'Sponsor Username'), Session::get('Backend_SponsorUsername')), 'Username');
			Session::clear('Backend_SponsorUsername');
			Session::clear('Backend_PlacementUsername');
			Session::clear('Backend_PlacementPosition');
        }
        else{
			if(Permission::check('CHANGE_Placement')){
        		$placement = Placement::get()->find('MemberID', $this->owner->ID);
				if(!$placement){
					$placement = Placement::create();
				}
        		$fields->insertAfter(PlacementField::create('SetPlacementUsername', _t('Network_MemberExtension.PLACEMENT_USERNAME', 'Placement Username'), $this->owner->Placement()->Username, $placement->NSeqno)->setExcludeID($placement->ID), 'Username');
        	}
			else{
	            $position = ($obj = Placement::get()->find('MemberID', $this->owner->ID)) ? $obj->NSeqno : 0;
	            $fields->insertAfter($placement = ReadonlyField::create('PlacementUsername', _t('Network_MemberExtension.PLACEMENT_USERNAME', 'Placement Username'), sprintf('%s - %s (%s)', $this->owner->Placement()->Username, $this->owner->Placement()->getName(), $position)), 'Username');
				$placement->setIncludeHiddenField(true);
			}

        	if(Permission::check('CHANGE_Sponsor')){
        		$fields->insertAfter(SponsorField::create('SetSponsorUsername', _t('Network_MemberExtension.SPONSOR_USERNAME', 'Sponsor Username'), $this->owner->Sponsor()->Username), 'Username');
        	}
			else{
            	$fields->insertAfter($sponsor = ReadonlyField::create('SponsorUsername', _t('Network_MemberExtension.SPONSOR_USERNAME', 'Sponsor Username'), sprintf('%s - %s', $this->owner->Sponsor()->Username, $this->owner->Sponsor()->getName())), 'Username');
				$sponsor->setIncludeHiddenField(true);
			}
        }
    }
    
    function onAfterWrite(){
    	if($this->owner->hasField('SetSponsorUsername')){
	        $root_username = Sponsor::get_root_username();
	        $sponsor = Sponsor::get()->find('MemberID', $this->owner->ID);
			if(!$sponsor){
				$sponsor = Sponsor::create()->setField('MemberID', $this->owner->ID);
			}
			
	        if($sponsor->Parent()->Member()->Username != $this->owner->SetSponsorUsername && $this->owner->Username != $root_username){
	            $sponsor->setSponsor($this->owner->SetSponsorUsername);
	            $sponsor->write();
				$sponsor->destroy();
	        }
		}
		
		if($this->owner->hasField('SetPlacementUsername') || $this->owner->hasField('SetSponsorUsername')){
	        $root_username = Placement::get_root_username();
	        $placement = Placement::get()->find('MemberID', $this->owner->ID);
			if(!$placement){
				$placement = Placement::create()->setField('MemberID', $this->owner->ID);
			}
			
	        if($this->owner->Username != $root_username){
	        	if($placement->Parent()->Member()->Username != $this->owner->SetPlacementUsername && $this->owner->hasField('SetPlacementUsername')){
	                $placement->setPlacement($this->owner->SetPlacementUsername);
	            }
	            else if(!$placement->exists() && $this->owner->hasField('SetSponsorUsername') && !Config::inst()->get('Placement', 'pending_placement')){
	                $placement->setPlacement($this->owner->SetSponsorUsername);
	            }
				
				if($this->owner->hasField('SetPlacementPosition')){
                    $placement->setPosition($this->owner->SetPlacementPosition);
                } 
                else if($this->owner->hasField('SetPlacementPositionOption')){
                    $placement->setPositionOption($this->owner->SetPlacementPositionOption);
                }
				
	            $placement->write();
				$placement->destroy();
	        }
        }
    }
    
    function Sponsor(){
		if($this->owner->SetSponsorUsername){
			$obj = Sponsor::get()->filter('Member.Username', $this->owner->SetSponsorUsername)->first();
			return $obj ? $obj->Member() : Member::create();
		}
		
		$sponsor = Sponsor::get()->find('MemberID', $this->owner->ID);
        return $sponsor ? $sponsor->Parent()->Member() : Member::create();
    }
    
    function getTotalSponsorDirectDownline(){
        $sponsor = Sponsor::get()->find('MemberID', $this->owner->ID);
        return ($sponsor) ? $sponsor->TotalDirectDownline : 0;
    }
    
    function getTotalSponsorGroupDownline(){
        $sponsor = Sponsor::get()->find('MemberID', $this->owner->ID);
        return ($sponsor) ? $sponsor->TotalGroupDownline : 0;
    }
	
	function Placement(){
		if($this->owner->SetPlacementUsername){
			$obj = Placement::get()->filter('Member.Username', $this->owner->SetPlacementUsername)->first();
			return $obj ? $obj->Member() : Member::create();
		}
		
		$placement = Placement::get()->find('MemberID', $this->owner->ID);
        return $placement ? $placement->Parent()->Member() : Member::create();
    }
    
    function getTotalPlacementDirectDownline(){
        $placement = Placement::get()->find('MemberID', $this->owner->ID);
        return ($placement) ? $placement->TotalDirectDownline : 0;
    }
    
    function getTotalPlacementGroupDownline(){
        $placement = Placement::get()->find('MemberID', $this->owner->ID);
        return ($placement) ? $placement->TotalGroupDownline : 0;
    }
	
	function getTotalPlacementLeftGroupDownline(){
        $placement = Placement::get()->find('MemberID', $this->owner->ID);
        return ($placement) ? $placement->TotalLeftGroupDownline : 0;
    }
	
	function getTotalPlacementRightGroupDownline(){
        $placement = Placement::get()->find('MemberID', $this->owner->ID);
        return ($placement) ? $placement->TotalRightGroupDownline : 0;
    }
	
	public function providePermissions() {
        return array(
            'CHANGE_Sponsor' => array(
                'name' => _t('Network_MemberExtension.PERMISSION_CHANGE_SPONSOR', 'Allow change sponsor'),
                'category' => _t('Network_MemberExtension.PERMISSIONS_CATEGORY', 'Network')
            ),
            'CHANGE_Placement' => array(
                'name' => _t('Network_MemberExtension.PERMISSION_CHANGE_PLACEMENT', 'Allow change placement'),
                'category' => _t('Network_MemberExtension.PERMISSIONS_CATEGORY', 'Network')
            )
        );
    }
}
