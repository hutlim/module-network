<?php

class Network_MemberAdminExtension extends Extension {
    function updateSearchContext($context) {
    	if($this->owner->modelClass == 'Member'){
        	$context->addField(TextField::create('q[SponsorUsername]', _t('Network_MemberAdminExtension.SPONSOR_USERNAME', 'Sponsor Username')));
		}
    }
	
	function updateList(&$list){
		if($this->owner->modelClass == 'Member'){
			$params = $this->owner->request->requestVar('q');
			if(isset($params['SponsorUsername']) && $params['SponsorUsername']){
				$list = $list->innerJoin('Sponsor', '"Member"."ID" = "Sponsor"."MemberID"')->innerJoin('Sponsor', '"Sponsor"."ParentID" = "Parent"."ID"', 'Parent')->innerJoin('Member', '"Parent"."MemberID" = "ParentMember"."ID"', 'ParentMember')->where(sprintf('"ParentMember"."Username" LIKE \'%%%s%%\'', $params['SponsorUsername']));
			}
		}
	}
}
