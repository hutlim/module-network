<?php
class ChangePlacementLog extends DataObject {
    private static $singular_name = "Change Placement Log";
    private static $plural_name = "Change Placement Logs";

	private static $db = array(
		'FromPosition' => 'Varchar',
		'ToPosition' => 'Varchar'
	);

    private static $has_one = array(
    	'FromPlacement' => 'Member',
    	'ToPlacement' => 'Member',
    	'UpdateBy' => 'Member',
    	'Member' => 'Member'
	);

    private static $searchable_fields = array(
    	'Member.Username',
        'FromPlacement.Username',
        'ToPlacement.Username',
        'FromPosition',
        'ToPosition'
    );

    private static $summary_fields = array(
        'Created',
        'Member.Username',
        'Member.Name',
        'FromPlacement.Username',
        'ToPlacement.Username',
        'FromPosition',
        'ToPosition',
        'UpdateBy.Username'
    );
	
	/**
     * @return array
     */
    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

		$labels['Member.Username'] = _t('ChangePlacementLog.USERNAME', 'Username');
		$labels['Member'] = _t('ChangePlacementLog.NAME', 'Name');
		$labels['Member.Name'] = _t('ChangePlacementLog.NAME', 'Name');
        $labels['FromPlacement.Username'] = _t('ChangePlacementLog.FROM_PLACEMENT_USERNAME', 'From Placement Username');
		$labels['FromPlacement'] = _t('ChangePlacementLog.FROM_PLACEMENT_NAME', 'From Placement Name');
        $labels['ToPlacement.Username'] = _t('ChangePlacementLog.TO_PLACEMENT_USERNAME', 'To Placement Username');
		$labels['ToPlacement'] = _t('ChangePlacementLog.TO_PLACEMENT_NAME', 'To Placement Name');
		$labels['FromPosition'] = _t('ChangePlacementLog.FROM_POSITION', 'From Placement Position');
		$labels['ToPosition'] = _t('ChangePlacementLog.TO_POSITION', 'To Placement Position');
        $labels['Created'] = _t('ChangePlacementLog.DATE', 'Date');
		$labels['UpdateBy.Username'] = _t('ChangePlacementLog.UPDATED_BY', 'Updated By');
		$labels['UpdateBy'] = _t('ChangePlacementLog.UPDATED_BY', 'Updated By');
		
		return $labels;
    }

	function onBeforeWrite() {
        parent::onBeforeWrite();

		if($this->MemberUsername) {
            $this->MemberID = Distributor::get_id_by_username($this->MemberUsername);
        }
		
        if($this->FromPlacementUsername) {
            $this->FromPlacementID = Distributor::get_id_by_username($this->FromPlacementUsername);
        }
        
        if($this->ToPlacementUsername) {
            $this->ToPlacementID = Distributor::get_username_by_id($this->ToPlacementUsername);
        }
		
		if(!$this->exists()){
			$this->UpdateByID = Member::currentUserID();
		}
    }
	
	function canCreate($member = null) {
        return false;
    }

    function canEdit($member = null) {
        return false;
    }

    function canDelete($member = null) {
        return false;
    }

}
?>