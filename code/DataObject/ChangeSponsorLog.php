<?php
class ChangeSponsorLog extends DataObject {
    private static $singular_name = "Change Sponsor Log";
    private static $plural_name = "Change Sponsor Logs";

    private static $has_one = array(
    	'FromSponsor' => 'Member',
    	'ToSponsor' => 'Member',
    	'UpdateBy' => 'Member',
    	'Member' => 'Member'
	);

    private static $searchable_fields = array(
    	'Member.Username',
        'FromSponsor.Username',
        'ToSponsor.Username'
    );

    private static $summary_fields = array(
        'Created',
        'Member.Username',
        'Member.Name',
        'FromSponsor.Username',
        'ToSponsor.Username',
        'UpdateBy.Username'
    );
	
	/**
     * @return array
     */
    function fieldLabels($includerelations = true) {
        $labels = parent::fieldLabels($includerelations);

		$labels['Member.Username'] = _t('ChangeSponsorLog.USERNAME', 'Username');
		$labels['Member'] = _t('ChangeSponsorLog.NAME', 'Name');
		$labels['Member.Name'] = _t('ChangeSponsorLog.NAME', 'Name');
        $labels['FromSponsor.Username'] = _t('ChangeSponsorLog.FROM_SPONSOR_USERNAME', 'From Sponsor Username');
		$labels['FromSponsor'] = _t('ChangeSponsorLog.FROM_SPONSOR_NAME', 'From Sponsor Name');
        $labels['ToSponsor.Username'] = _t('ChangeSponsorLog.TO_SPONSOR_USERNAME', 'To Sponsor Username');
		$labels['ToSponsor'] = _t('ChangeSponsorLog.TO_SPONSOR_NAME', 'To Sponsor Name');
        $labels['Created'] = _t('ChangeSponsorLog.DATE', 'Date');
		$labels['UpdateBy.Username'] = _t('ChangeSponsorLog.UPDATED_BY', 'Updated By');
		$labels['UpdateBy'] = _t('ChangeSponsorLog.UPDATED_BY', 'Updated By');
		
		return $labels;
    }

	function onBeforeWrite() {
        parent::onBeforeWrite();

		if($this->MemberUsername) {
            $this->MemberID = Distributor::get_id_by_username($this->MemberUsername);
        }
		
        if($this->FromSponsorUsername) {
            $this->FromSponsorID = Distributor::get_id_by_username($this->FromSponsorUsername);
        }
        
        if($this->ToSponsorUsername) {
            $this->ToSponsorID = Distributor::get_username_by_id($this->ToSponsorUsername);
        }
		
		if(!$this->exists()){
			$this->UpdateByID = Member::currentUserID();
		}
    }
	
	function canCreate($member = null) {
        return false;
    }

    function canEdit($member = null) {
        return false;
    }

    function canDelete($member = null) {
        return false;
    }

}
?>