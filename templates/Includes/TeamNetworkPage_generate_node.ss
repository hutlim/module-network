<% if $Member.ID > 0 %>
	<a class="member_detail" rel="tooltip" href="$MemberLink" title="<%t TeamNetworkPage_generate_node.ss.VIEW_MEMBER_DETAILS "Click here to view member details {username}" username=$Member.Username %>" data-title="<%t TeamNetworkPage_generate_node.ss.MEMBER_DETAILS "Member Details" %>" data-username="$Member.Username"><img src="$Image" width="36" height="45" border="0"><br>$Member.Username</a>
	<br>
	<%t TeamNetworkPage_generate_node.ss.POINT "Left: {left} | Right: {right}" left=$Member.TodayTotalLeftGroupBV.Formatted  right=$Member.TodayTotalRightGroupBV.Formatted %>
<% else_if $ParentID > 0 && $Link != '' %>
	<a href="$Link" title="<%t TeamNetworkPage_generate_node.ss.REGISTER_NEW_MEMBER "Click here to register new team member" %>" class="register" rel="tooltip">
		<img src="$Image" width="36" height="45" border="0">
		<br>
		<%t TeamNetworkPage_generate_node.ss.BUTTONREGISTER "Register" %>
	</a>
<% else %>
	<img src="$Image" width="36" height="45" border="0">
<% end_if %>