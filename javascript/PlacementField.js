(function($) {
	$(':text[rel~=placement-username]').live("focusin", function(e) {
		e.preventDefault();
		var field = $(this);
		var form_container = field.parents('form');
		var sponsor_container = form_container.find("div.field.sponsor");
		var sponsor_id = sponsor_container.find('input[rel~=sponsor-id]').val();
		if(sponsor_id == '' || sponsor_id <= 0){
			alert(ss.i18n._t('PlacementField.SPONSOR_REQUIRED', 'Sponsor username is required'));
            sponsor_container.find(':text[rel~=sponsor-username]').val('').focus();
		}
	});
	
	$(':text[rel~=placement-username]').live("change", function(e) {
		e.preventDefault();
		var field = $(this);
		var form_container = field.parents('form');
		var sponsor_container = form_container.find("div.field.sponsor");
		var sponsor_id = sponsor_container.find('input[rel~=sponsor-id]').val();
		var placement_container = field.parents('div.field.placement');
		var placement_username = field.val();
		var placement_position = placement_container.find('select[rel~=placement-position]').val();
		if(placement_username == ''){
			placement_container.find('input[rel~=placement-id]').val('');
			placement_container.find('input[rel~=placement-name]').val('');
			placement_container.find('select[rel~=placement-position]').val('').trigger('liszt:updated');
			return false;
		}
		if(sponsor_id > 0){
			$.ajax({
				url: $(this).data('url'),
	          	dataType: 'json',
	          	data: {'username': placement_username, 'position': placement_position, 'sponsor_id': sponsor_id},
	          	beforeSend: function(xhr) {
	          		form_container.find('input[type="submit"], input[type="button"], button').prop('disabled', true);
					sponsor_container.find(':text[rel~=sponsor-username]').prop('disabled', true);
					placement_container.find('select[rel~=placement-position]').prop('disabled', true).trigger('liszt:updated');
					field.prop('disabled', true).addClass('loading');
	          	},
	          	spinner: false
	        })
			.always(function() {
				form_container.find('input[type="submit"], input[type="button"], button').prop('disabled', false);
				sponsor_container.find(':text[rel~=sponsor-username]').prop('disabled', false);
				placement_container.find('select[rel~=placement-position]').prop('disabled', false).trigger('liszt:updated');
				field.prop('disabled', false).removeClass('loading');
	  		})
			.done(function(data) {
			    if(data.result == 'success'){
			    	placement_container.find('input[rel~=placement-id]').val(data.id);
			    	placement_container.find('input[rel~=placement-name]').val(data.name);
			    } else {
			    	alert(data.msg);
			    	if(data.result == 'username_error'){
			    		placement_container.find('input[rel~=placement-id]').val('');
			    		placement_container.find('input[rel~=placement-name]').val('');
			    		field.val('').focus();
			    	}
			    	else if(data.result == 'position_error'){
			    		placement_container.find('input[rel~=placement-id]').val(data.id);
			    		placement_container.find('input[rel~=placement-name]').val(data.name);
						placement_container.find('select[rel~=placement-position]').val('').trigger('liszt:updated');
						placement_container.find('select[rel~=placement-position]').focus();
			    	}
			    	else{
			    		placement_container.find('input[rel~=placement-id]').val('');
			    		placement_container.find('input[rel~=placement-name]').val('');
			    						placement_container.find('select[rel~=placement-position]').val('').trigger('liszt:updated');
			    		field.val('').focus();
			    	}
			    }
			})
			.fail(function( jqxhr, textStatus, error ) {
				alert(ss.i18n._t('PlacementField.ERROR_LOADING', 'Error occur while loading, please try again.'));
			    placement_container.find('input[rel~=placement-id]').val('');
			    placement_container.find('input[rel~=placement-name]').val('');
			    field.val('').focus();
			});
		}
		else{
			alert(ss.i18n._t('PlacementField.SPONSOR_REQUIRED', 'Sponsor username is required'));
			sponsor_container.find(':text[rel~=sponsor-username]').val('').focus();
		}
	});
	
	$('select[rel~=placement-position]').live("change", function(e) {
		e.preventDefault();
		var field = $(this);
		var form_container = field.parents('form');
		var sponsor_container = field.parents('form').find("div.field.sponsor");
		var sponsor_id = sponsor_container.find('input[rel~=sponsor-id]').val();
		var placement_container = field.parents('div.field.placement');
		var placement_position = field.val();
		if(placement_position == '' || !/^[0-9]+$/.test(placement_position)){
			return false;
		}

		if(sponsor_id > 0){
			var placement_id = field.parents('form').find('input[rel~=placement-id]').val();
			if(placement_id > 0){
				$.ajax({
					url: $(this).data('url'),
		          	dataType: 'json',
		          	data: {'position': placement_position, 'placement_id': placement_id},
		          	beforeSend: function(xhr) {
		          		form_container.find('input[type="submit"], input[type="button"], button').prop('disabled', true);
						sponsor_container.find(':text[rel~=sponsor-username]').prop('disabled', true);
						field.prop('disabled', true).trigger('liszt:updated');
						placement_container.find(':text[rel~=placement-username]').prop('disabled', true).addClass('loading');
		          	},
		          	spinner: false
		        })
				.always(function() {
					form_container.find('input[type="submit"], input[type="button"], button').prop('disabled', false);
					sponsor_container.find(':text[rel~=sponsor-username]').prop('disabled', false);
					field.prop('disabled', false).trigger('liszt:updated');
					placement_container.find(':text[rel~=placement-username]').prop('disabled', false).removeClass('loading');
				})
				.done(function(data) {
				    if(data.result != 'success'){
				    	alert(data.msg);
				    	field.val('').trigger('liszt:updated');
				    	field.focus();
				    }
				})
				.fail(function( jqxhr, textStatus, error ) {
					alert(ss.i18n._t('PlacementField.ERROR_LOADING', 'Error occur while loading, please try again.'));
				    field.val('').trigger('liszt:updated');
				    field.focus();
				});
			} else {
				alert(ss.i18n._t('PlacementField.PLACEMENT_REQUIRED', 'Placement username is required'));
				field.val('').trigger('liszt:updated');
				placement_container.find('input[rel~=placement-id]').val('');
				placement_container.find('input[rel~=placement-name]').val('');
				placement_container.find(':text[rel~=placement-username]').val('').focus();
			}
		}
		else{
			alert(ss.i18n._t('PlacementField.SPONSOR_REQUIRED', 'Sponsor username is required'));
			sponsor_container.find(':text[rel~=sponsor-username]').val('').focus();
		}
	});
})(jQuery);
