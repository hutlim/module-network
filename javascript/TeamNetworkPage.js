(function($) {
	$(document).ready(function() {
		$('#search_tree').live('click', function(event) {
			var username = $('#search_text').val();
			if(username != '' && username != undefined){
				doRefreshTree(username, $('#search_text').attr('data-link'));
			}
			event.preventDefault();
		});
		
		// start modal div
		var content = '<div id="member_detail_modal" class="modal">';
		
		// start dialog
		content += '<div class="modal-dialog">';
		
		// start content
		content += '<div class="modal-content">';
		
		// header
		content += '<div class="modal-header">';
        content += '<h4 class="modal-title" style="float: left;">&nbsp</h4>';
		content += '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
		content += '</div>';
		
		// body
		content += '<div class="modal-body"></div>';
		
		// footer
		content += '<div class="modal-footer"></div>';
		
		// close content
		content += '</div>';
		
		// close dialog
		content += '</div>';
		
		// close modal div
		content += '</div>';
		$('body').append(content);
		
		var $member_detail_modal = $('#member_detail_modal');
		var $member_detail_modal_title = $member_detail_modal.find('.modal-title');
		var $member_detail_modal_body = $member_detail_modal.find('.modal-body');
		var $member_detail_modal_footer = $member_detail_modal.find('.modal-footer');
		
		$member_detail_modal.on('shown.bs.modal', function () {
			$member_detail_modal_body.find('form').ajaxForm({ 
				delegation: true,
	    		target: '#member_detail_modal .modal-body',
		        'beforeSubmit' : function(arr, form, options) { 
					$('.holder-required').removeClass('holder-required');
					$('span.message').remove();
				},
		        'success' : function(data){
	
		        }
		    });
		});
		
		$member_detail_modal.on('hide.bs.modal', function () {
			$member_detail_modal_title.html();
			$member_detail_modal_body.html();
		});
		
		function doRefreshTree(username, link){
			var url = link + '/' + username;
			$.getJSON(url, function(data) {
				if(data.error != '' && data.error != undefined){
					alert(data.error);
				}
				else{
			    	$('#tree').html(data.result);
			   	}
			    
			}).fail(function() {
				alert(ss.i18n._t('TeamNetworkPage.ERROR_LOADING_TREE', 'Error occur while load these tree, please try again.'));
			});
		}
		
		/* Init */
		var $searchtextfield = $('#search_text');
		doRefreshTree($searchtextfield.val(), $searchtextfield.data('link'));
		
		$('#previous').live('click', function(event) {
			var username = $(this).attr('data-username');
			if(username != '' && username != undefined){
				$('#search_text').val(username);
				doRefreshTree(username, $('#search_text').data('link'));
			}
			event.preventDefault();
		});
    	
		$('.member_detail').live('click', function(event) {
			var url = this.href;
			var title = $(this).data('title');
			var username = $(this).attr('data-username');
			
			if($(this).data('close-reload') == 'true'){
				$member_detail_modal.on('hide.bs.modal', function () {
					document.location.reload(true);
				});
			}
			
			if (url.indexOf('#') == 0) {
				$(url).modal('open');
			}
			else {
				$member_detail_modal.on('show.bs.modal', function () {
					$member_detail_modal_title.text(title);
				});
				
				$.get(url, function(data) {
				    $member_detail_modal_body.html(data);
				    var footer = '<p class="text-right"><a class="view_tree btn btn-primary" href="#" data-username="' + username + '">' + $('#tree').data('view-team-button') + '</a></p>';
				    $member_detail_modal_footer.html(footer);
				    $member_detail_modal.modal('show');
				}).fail(function() {
					$member_detail_modal_body.html('<p>' + ss.i18n._t('TeamNetworkPage.ERROR_LOADING_MEMBER_DETAILS', 'Error occur while load member details, please try again.') +'</p>');
					$member_detail_modal.modal('show');
				});
			}
			
			event.preventDefault();
		});
		
		$('a.view_tree').live('click', function(event) {
	    	$('#member_detail_modal').modal('hide');
			var username = $(this).attr('data-username');
			if(username != '' && username != undefined){
				$('#search_text').val(username);
				doRefreshTree(username, $('#search_text').attr('data-link'));
			}
			event.preventDefault();
		});
	});
})(jQuery);