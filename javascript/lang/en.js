if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('en', {
		'SponsorField.ERROR_LOADING' : 'Error occur while loading, please try again.',
		'PlacementField.ERROR_LOADING' : 'Error occur while loading, please try again.',
		'PlacementField.SPONSOR_REQUIRED' : 'Sponsor username is required',
		'PlacementField.PLACEMENT_REQUIRED' : 'Placement username is required',
		'TeamMemberField.ERROR_LOADING' : 'Error occur while loading, please try again.',
		'TeamNetworkPage.ERROR_LOADING_TREE' : 'Error occur while load these tree, please try again.',
		'TeamNetworkPage.ERROR_LOADING_MEMBER_DETAILS' : 'Error occur while load member details, please try again.',
		'PersonalSponsorPage.MEMBER_DETAILS' : 'Member Details',
		'PersonalSponsorPage.ERROR_LOADING_TREE' : 'Error occur while load these tree, please try again.',
		'PersonalSponsorPage.ERROR_LOADING_MEMBER_DETAILS' : 'Error occur while load member details, please try again.',
		'NetworkAdmin.LOADING' : 'Loading...',
		'NetworkAdmin.ERROR_LOADING_TREE' : 'Error occur while load these tree, please try again.',
		'NetworkAdmin.ERROR_LOADING_MEMBER_DETAILS' : 'Error occur while load member details, please try again.',
	});
}