if ( typeof (ss) == 'undefined' || typeof (ss.i18n) == 'undefined') {
	console.error('Class ss.i18n not defined');
} else {
	ss.i18n.addDictionary('zh', {
		'SponsorField.ERROR_LOADING' : '加载时发生错误，请重试。',
		'PlacementField.ERROR_LOADING' : '加载时发生错误，请重试。',
		'PlacementField.SPONSOR_REQUIRED' : '推荐人为必填项',
		'PlacementField.PLACEMENT_REQUIRED' : '安置会员为必填项',
		'TeamMemberField.ERROR_LOADING' : '加载时发生错误，请重试。',
		'TeamNetworkPage.ERROR_LOADING_TREE' : '加载安置组织图时发生错误，请重试。',
		'TeamNetworkPage.ERROR_LOADING_MEMBER_DETAILS' : '加载会员资料时发生错误，请重试。',
		'PersonalSponsorPage.MEMBER_DETAILS' : '会员详细信息',
		'PersonalSponsorPage.ERROR_LOADING_TREE' : '加载经营组织图时发生错误，请重试。',
		'PersonalSponsorPage.ERROR_LOADING_MEMBER_DETAILS' : '加载会员资料时发生错误，请重试。',
		'NetworkAdmin.LOADING' : '加载中。。。',
		'NetworkAdmin.ERROR_LOADING_TREE' : '加载安置组织图时发生错误，请重试。',
		'NetworkAdmin.ERROR_LOADING_MEMBER_DETAILS' : '加载会员资料时发生错误，请重试。',
	});
}